public class Monkey {
	//Fields
	private String type;
	private String hasTail;
	private String color;
	private boolean isClean;
	
	//Field getters
	public String getType() {
		return this.type;
	}
	public String getTail() {
		return this.hasTail;
	}
	public String getColor() {
		return this.color;
	}
	public boolean getIsClean() {
		return this.isClean;
	}
	
	//Field setters
	public void setType(String newType){
		this.type = newType;
	}
	public void setHasTail(String newTail){
		this.hasTail = newTail;
	}
	public void setColor(String newColor){
		this.color = newColor;
	}
	
	//Field constructor
	
	public Monkey(String type, String hasTail, String color){
		this.type = type;
		this.hasTail = hasTail;
		this.color = color;
	}
	//Actions
	public void peelBanana(){
		System.out.println(this.type + " has peeled a banana!(which has suddenly disappeared...)");
	}
	public void swingFromTree(){
		if(this.hasTail.equals("yes")) {
			System.out.println(this.type + " is swinging through the treets with tail!");
		}
		else {
			System.out.println(this.type + " is swinging through the trees with hands and feet!");
		}
	}
	public void getClean(String smell){
		if(validateVal(smell)){
			this.isClean = true;
		}
		else {
			System.out.println("Invalid entry: Please enter yes or no");

		}
	
	}
	
	private boolean validateVal(String smell){
		boolean validVal = true; 
		if(smell.equals("yes")){
			System.out.println(this.type + " is picking bugs out of their fur!");
			return validVal;
		}
	
		else if(smell.equals("no")){
			System.out.println(this.type + " is already clean!");
			return validVal;
		
		}
		else{
			return validVal = false;
		}
	}
}