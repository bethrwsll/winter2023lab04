import java.util.Scanner;
public class NationalPark 
{
	public static void main(String[] args) 
	{
		Scanner scan = new Scanner(System.in);
		Monkey[] troop = new Monkey[4];
		
		//Creating troop
		for(int i = 0; i < troop.length; i++)
			{
			System.out.println("Enter a type of monkey " + (i+1) + ": ");
			String type = scan.nextLine();
			System.out.println("Does monkey " + (i+1) + " have a tail: ");
			String hasTail = scan.nextLine();
			System.out.println("Enter a color of monkey " + (i+1) + ": ");
			String color = scan.nextLine();
			troop[i] = new Monkey(type, hasTail, color);
			}
		
		//Print monkey 4 fields
		System.out.println("Monkey 4: Type: " + troop[3].getType() + ", Has tail: " + troop[3].getTail() + ", Color: " + troop[3].getColor());
		//Change monkey 4 fields with setters
		System.out.println("Change monkey 4 type:");
			String type = scan.nextLine();
			troop[3].setType(type);
		System.out.println("Does new monkey 4 have a tail?");
			String hasTail = scan.nextLine();
			troop[3].setHasTail(hasTail);
		System.out.println("Change monkey 4 color:");
			String color = scan.nextLine();
			troop[3].setColor(color);
		//Re-print monkey 4 fields
		System.out.println("Monkey 4: Type: "+ troop[3].getType() + ", Has tail: " + troop[3].getTail() + ", Color: " + troop[3].getColor());

		//Calling Monkey 1 actions
		troop[0].peelBanana();
		troop[0].swingFromTree();
		
		//Calling Monkey 2 actions
		System.out.println("Does Monkey 2 smell?");
		String smell = scan.nextLine();
		troop[1].getClean(smell);
	}
}
